<?php
//
function array_remove_keys(&$ar, $child)
{
    if (!is_array($ar)) return;
    $keys = array_keys($ar);
    $is_assoc = $keys !== range(0, count($ar) - 1);
    $could_sort = true;
    for ($i = 0; $i < count($keys); $i++) {
        if (is_string($keys[$i])) {
            $could_sort = false;
            break;
        }
    }
    if ($is_assoc && $could_sort) sort($ar);
    foreach ($ar as $k => &$v) {
        if (is_array($v)) array_remove_keys($v, $child);
        if ($k === $child) sort($v);
    }
    return $ar;
}