<?php

namespace app\api\controller;

use \think\Loader;
use app\common\controller\Auth as AuthController;

class User extends AuthController
{
    protected $userEntryLogic;
    protected $userProfileLogic;
    protected $userBuyLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->userEntryLogic = Loader::model('UserEntry', 'logic');
        $this->userProfileLogic = Loader::model('UserProfile', 'logic');
        $this->userBuyLogic = Loader::model('UserBuy', 'logic');
    }

    /**
     *  用户注册
     * @param $account，用户帐号， $password，用户密码
     * @return customizedResult
     */
    public function regist($account, $password)
    {
        $input = [
            'account' => $account,
            'password' => $password
        ];
        $validateInfo = $this->validate($input, 'User.regist');
        if ($validateInfo === true) {
            $data = $this->userEntryLogic->regist($input);
            if (isset($data['userId'])) {
                $output = array(
                    'token' => $this->createToken($data['userId'])
                );
                $this->customizedResult(true, $output, '注册成功', '10000');
            }
        }
    }

    /**
     *  用户登录
     * @param $account，用户帐号， $password
     * @return customizedResult
     */
    public function login($account, $password = '')
    {
        $input = [
            'account' => $account,
            'password' => $password
        ];
        $data = $this->userEntryLogic->login($input);
        if (isset($data['userId'])) {
            $output = array(
                'token' => $this->createToken($data['userId'])
            );
            $this->customizedResult(true, $output, '登录成功', '10000');
        } else {
            $this->customizedResult(false, null, $data, '10010');
        }

    }

    /**
     *  获取用户的基本信息
     * @param null
     * @return customizedResult
     */
    public function baseInfo()
    {
        $data = $this->userProfileLogic->baseInfo();
        if (!empty($data)) {
            $this->customizedResult(true, $data, '获取用户信息成功', '10000');
        }
    }

    /**
     *  获取用户的账户信息
     * @param null
     * @return customizedResult
     */
    public function accountInfo()
    {
        $data = $this->userProfileLogic->accountInfo();
        if (!empty($data)) {
            $this->customizedResult(true, $data, '获取用户账户信息成功', '10000');
        }
    }

    /**
     *  获取用户的追踪信息
     * @param null
     * @return customizedResult
     */
    public function trackInfo()
    {
        $data = $this->userProfileLogic->trackInfo();
        if (!empty($data)) {
            $this->customizedResult(true, $data, '获取用户追踪信息成功', '10000');
        }
    }

    /**
     *  编辑用户信息
     * @param $type ,编辑类型,$value,编辑值
     * @return customizedResult
     */
    public function editBaseInfo($type, $value)
    {
        if ($type === 'nickName') {
            $data = $this->userProfileLogic->editNickName($value);
        } else if ($type === 'realName') {
            $data = $this->userProfileLogic->editRealName($value);
        } else if ($type === 'avatar') {
            $data = $this->userProfileLogic->editAvatarByBase64($value);
        } else {
            $data = 'type字段为非法值';
        }
        $this->customizedResult(true, $data, '获取用户信息成功', '10000');
    }

    /**
     *  完善用户资料
     * @param $avatar ,用户头像,$nickName,用户昵称,$phone,用户手机号码
     * @return bool
     */
    public function completeBaseInfo($avatar, $nickName, $phone)
    {
        $data = $this->userProfileLogic->completeBaseInfo($avatar, $nickName, $phone);
        $this->customizedResult(true, $data, '完善用户基本信息成功', '10000');
    }

    /**
     *  获取用户的地址列表
     * @param null
     * @return customizedResult
     */
    public function addressList()
    {
        $data = $this->userProfileLogic->addressList();
        if (isset($data)) {
            $this->customizedResult(true, $data, '获取用户地址列表成功', '10000');
        }
    }

    /**
     *  用户添加地址
     * @param $receiver ,收件人,$phone,手机号码,$provinceId,省份ID,etc
     * @return bool
     */
    public function addAddress($receiver, $phone, $provinceId, $cityId, $areaId, $addressDetails)
    {
        $input = [
            'receiver' => $receiver,
            'phone' => $phone,
            'province_id' => $provinceId,
            'city_id' => $cityId,
            'area_id' => $areaId,
            'address' => $addressDetails
        ];
        $data = $this->userProfileLogic->addAddress($input);
        if ($data) {
            $this->customizedResult(true, null, '添加用户地址成功', '10000');
        }
    }

    /**
     *  获取用户购物车列表
     * @param null
     * @return customizedResult
     */
    public function cart()
    {
        $data = $this->userBuyLogic->cart();
        $this->customizedResult(true, $data, '获取用户购物车列表成功', '10000');
    }

    /**
     *  更新购物车产品数量
     * @param $num
     * @param $productId
     * @param $cartId
     * @return customizedResult
     */
    public function updateCartProductNum($num, $productId, $cartId)
    {
        $result = $this->userBuyLogic->updateCartProductNum($num, $productId, $cartId);
        if ($result) {
            $this->customizedResult(true, $result, '更新用户购物车产品数量成功', '10000');
        } else {
            $this->customizedResult(false, $result, '更新用户购物车产品数量失败', '10100');
        }
    }

    /**
     *  获取订单确认页面信息
     * @param null
     * @return customizedResult
     */
    public function orderFirmInfo($ids)
    {
        $data = $this->userBuyLogic->orderFirmInfo($ids);
        $this->customizedResult(true, $data, '获取订单确认页面信息成功', '10100');
    }

    /**
     *  添加订单
     * @param null
     * @return customizedResult
     */
    public function orderSubmit()
    {
        $field = [
            'product' => request()->param('product/a'),
            'shippingId' => request()->param('shippingId'),
            'expressId' => request()->param('expressId'),
            'peas' => request()->param('peas'),
            'couponId' => request()->param('couponId'),
            'payId' => request()->param('payId')
        ];
        $data = $this->userBuyLogic->orderSubmit($field);
        $this->customizedResult(true, $data, '提交订单成功', '10100');
    }

    /**
     *  订单支付信息
     * @param $
     * @return array
     */
    public function orderPayInfo($id)
    {
        $data = $this->userBuyLogic->orderPayInfo($id);
        $this->customizedResult(true, $data, '获取支付订单信息成功', '10100');
    }

}