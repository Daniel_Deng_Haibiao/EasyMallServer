<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/27
 * Time: 8:55
 */

namespace app\api\controller;

use \think\Loader;
use app\common\controller\Base as BaseController;

class Mall extends BaseController
{
    protected $mallLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->mallLogic = Loader::model('Mall', 'logic');
    }

    /**
     *  获取商城热卖列表
     * @param null
     * @return customizedResult
     */
    public function hotGoods()
    {
        $list = $this->mallLogic->hotGoods();
        if (!empty($list)) {
            $this->customizedResult(true, $list, '获取商城热卖列表', '10000');
        }
    }

    /**
     *  获取商品分类
     * @param null
     * @return customizedResult
     */
    public function category()
    {
        $list = $this->mallLogic->category();
        $this->customizedResult(true, $list, '获取商城分类列表成功', '10000');
    }
}
