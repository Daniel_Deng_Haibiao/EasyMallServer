<?php

namespace app\api\controller;

use \think\Loader;
use app\common\controller\Base as BaseController;

class Open extends BaseController
{
    protected $openLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->openLogic = Loader::model('Open', 'logic');
    }

    /**
     *  获取全国地址列表
     * @param null
     * @return array
     */
    public function addressList()
    {
        $list = $this->openLogic->addressList();
        if (!empty($list)) {
            $this->customizedResult(true, $list, '获取地址列表成功', '10000');
        }
    }
}
