<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 16:47
 */

namespace app\api\controller;

use think\Loader;
use app\common\controller\Auth as AuthController;

class Wechat extends AuthController
{
    protected $wechatEntryLogic;
    protected $wechatJsSdkService;
    protected $wechatReceiveService;

    public function _initialize()
    {
        parent::_initialize();
        $this->wechatEntryLogic = Loader::model('WechatEntry', 'logic');
        $this->wechatJsSdkService = Loader::model('WechatJsSdk', 'service');
        $this->wechatReceiveService = Loader::model('WechatReceive', 'service');
    }

    /**
     *  微信服务器信息推送
     * @param null
     * @return null
     */
    public function receive()
    {
        $this->wechatReceiveService->receive();
    }

    /**
     *  微信网页js-sdk签名
     * @param $url
     * @return customizedResult
     */
    public function jsSignature($url)
    {
        $data = $this->wechatJsSdkService->signature($url);
        if (!empty($data)) {
            $this->customizedResult(true, $data, '获取网页签名成功', '10000');
        }
    }

    /**
     *  微信登陆
     * @param $account
     * @return customizedResult
     */
    public function login($account)
    {
        $data = $this->wechatEntryLogic->login($account);
        if (isset($data['userId'])) {
            $output = array(
                'token' => $this->createToken($data['userId'])
            );
            $this->customizedResult(true, $output, '登录成功', '10000');
        }
    }
}