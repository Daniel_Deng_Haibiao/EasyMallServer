<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/27
 * Time: 9:04
 */

namespace app\api\logic;

use think\Model;
use app\common\model\Goods as GoodsModel;
use app\common\model\GoodsCategory as GoodsCategoryModel;

class Mall extends Model
{
    /**
     *  获取商城热卖列表
     * @param null
     * @return array
     */
    public function hotGoods()
    {
        $list = GoodsModel::all(function ($query) {
            $query->where(['is_hot' => 1])
                ->field("id,primary_pic_url as imgUrl, goods_brief as brief, market_price as price, counter_price as counterPrice, '92%' as rate")
                ->order('id', 'asc');
        });
        if (!empty($list)) {
            return $list;
        }
        return false;
    }

    /**
     *  获取商品分类
     * @param null
     * @return array
     */
    public function category()
    {
        $list = GoodsCategoryModel::all(function ($query) {
            $query->field('id, parent_id as pid, name, category_banner_url as bannerUrl, category_list_url as imgUrl');
        });
        return $list;
    }
}