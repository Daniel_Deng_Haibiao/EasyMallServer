<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2017/11/15
 * Time: 20:45
 */

namespace app\api\logic;

use think\Model;
use \think\Loader;

class Open extends Model
{
    protected $addressProvinceModel;
    protected function initialize()
    {
        $this->addressProvinceModel = Loader::model('AddressProvince', 'model');
    }

    /**
     *  获取全国地址列表
     * @param null
     * @return array
     */
    public function addressList()
    {
        $list = $this->addressProvinceModel->nationAddessList();
        if(!empty($list)){
            return $list;
        }
        return false;
    }
}