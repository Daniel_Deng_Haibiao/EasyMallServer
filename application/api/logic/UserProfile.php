<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 11:34
 */

namespace app\api\logic;

use think\Model;
use \think\Loader;
use app\common\utils\Image as ImageUtil;

class UserProfile extends Model
{
    protected $userModel;

    protected function initialize()
    {
        parent::initialize();
        $this->userModel = Loader::model('User', 'model');
    }

    /**
     *  获取用户基本信息
     * @param null
     * @return array
     */
    public function baseInfo()
    {
        $user = $this->userModel->currentUser();
        if (isset($user)) {
            $data = array(
                'nickName' => $user->user_nick_name,
                'userAvatar' => $user->user_avatar,
                'realName' => $user->user_real_name,
                'levelName' => $user->userLevel->userLevelDefine->definition,
                'levelValue' => $user->userLevel->level_value,
            );
            return $data;
        }
        return false;
    }

    /**
     *  获取用户账户信息
     * @param null
     * @return array
     */
    public function accountInfo()
    {
        $user = $this->userModel->currentUser();
        if (isset($user)) {
            $data = array(
                'peas' => $user->userPeas->peas_num,
                'frezenPeas' => $user->userPeas->frezen_peas_num,
                'balance' => $user->userAccount->balance,
                'frezenBalance' => $user->userAccount->frezen_balance
            );
            return $data;
        }
        return false;
    }

    /**
     *  获取用户追踪信息
     * @param null
     * @return array
     */
    public function trackInfo()
    {
        $user = $this->userModel->currentUser();
        if (isset($user)) {
            $data = array(
                'goods' => count($user->goodsCollect)
            );
            return $data;
        }
        return false;
    }

    /**
     *  编辑用户头像
     * @param $val ,用户头像（base64）
     * @return string
     */
    public function editAvatarByBase64($val)
    {
        $user = $this->userModel->currentUser();
        $image = new ImageUtil();
        $image->loadBase64($val)->save();
        if ($image->imageUrl) {
            $user->user_avatar = $image->imageUrl;
            if ($user->save()) {
                return $image->imageUrl;
            };
        }
        return '图片保存失败';
    }

    /**
     *  编辑用户昵称
     * @param $val ,用户昵称
     * @return string
     */
    public function editNickName($val)
    {
        $user = $this->userModel->currentUser();
        $user->user_nick_name = $val;
        if ($user->save()) {
            return $user->user_nick_name;
        }
        return '昵称修改失败';
    }

    /**
     *  完善用户资料
     * @param $avatar ,用户头像,$nickName,用户昵称,$phone,用户手机号码
     * @return array
     */
    public function completeBaseInfo($avatar, $nickName, $phone)
    {
        $image = new ImageUtil();
        $user = $this->userModel->currentUser();
        $user->user_nick_name = $nickName;
        $user->user_phone = $phone;
        $image->loadBase64($avatar)->save();
        if ($image->imageUrl) {
            $user->user_avatar = $image->imageUrl;
            if ($user->save()) {
                $data = [
                    'avatar' => $user->user_avatar,
                    'nickName' => $user->user_nick_name,
                    'phone' => $user->user_phone
                ];
                return $data;
            };
        }
    }

    /**
     *  编辑用户姓名
     * @param $val ,用户姓名
     * @return string
     */
    public function editRealName($val)
    {
        $user = $this->userModel->currentUser();
        $user->user_real_name = $val;
        if ($user->save()) {
            return $user->user_real_name;
        }
        return '姓名修改失败';
    }

    /**
     *  获取用户地址列表
     * @param null
     * @return string
     */
    public function addressList()
    {
        $list = $this->userModel->addressList();
        if (isset($list)) {
            return $list;
        }
        return '用户地址列表获取失败';
    }

    /**
     *  用户添加地址
     * @param $val ,前端提交数组
     * @return bool
     */
    public function addAddress($val)
    {
        $user = $this->userModel->currentUser();
        if ($user->userShippingAddress()->save($val)) {
            return true;
        }
    }

    /**
     *  获取用户默认收货地址
     * @param null
     * @return array
     */
    public function defaultAddress()
    {
        $user = $this->userModel->currentUser();
        $data = $user->userShippingAddress()
            ->with(['addressProvince', 'addressCity', 'addressArea'])
            ->where(['is_default' => 1])
            ->find();
        return $data;
    }
}