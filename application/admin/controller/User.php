<?php
/**
 * Created by PhpStorm.
 * User: daniel.deng.haibiao
 * Date: 2017/12/19
 * Time: 15:19
 */
namespace app\admin\controller;

use \think\Loader;
use app\common\controller\Auth as AuthController;

class User extends AuthController
{
    protected $userEntryLogic;
    
    public function _initialize()
    {
        parent::_initialize();
        $this->userEntryLogic = Loader::model('UserEntry', 'logic');
    }

    /**
     *  后台管理员登录
     * @param $account,帐号,$password,密码
     * @return object
     */
    public function login($account, $password)
    {
        $input = [
            'account' => $account,
            'password' => $password
        ];
        $data = $this->userEntryLogic->login($input);
        if (isset($data['userId'])) {
            $output =[
                'token' => $this->createToken($data['userId'])
            ];
            $this->customizedResult(true, $output, '登录成功', '10000');
        }
        $this->customizedResult(false, null, '登录失败', '10100');
    }
}