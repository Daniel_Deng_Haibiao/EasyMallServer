<?php

namespace app\admin\controller;

use \think\Loader;
use app\common\controller\Base as BaseController;

class Goods extends BaseController
{
    protected $goodsSingleLogic;
    protected $goodsSetLogic;
    protected $goodsNatureLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->goodsSingleLogic = Loader::model('GoodsSingle', 'logic');
        $this->goodsSetLogic = Loader::model('GoodsSet', 'logic');
        $this->goodsNatureLogic = Loader::model('GoodsNature', 'logic');
    }

    /**
     * @param null $isOnsale
     * @param null $isNew
     *  获取商品列表
     * @return array
     */
    public function ls($isOnsale = null, $isNew = null)
    {
        $data = $this->goodsSetLogic->ls();
        $this->customizedResult(true, $data, '获取商品列表成功', '10000');
    }

    /**
     *  商品分类
     * @panram null
     * @return arrray
     */
    public function category()
    {
        $data = $this->goodsNatureLogic->category();
        $this->customizedResult(true, $data, '获取商品分类成功', '10000');
    }

    /**
     * @descrription 商品品牌
     * @param null
     * @return array
     */
    public function brand()
    {
        $data = $this->goodsNatureLogic->brand();
        $this->customizedResult(true, $data, '获取商品品牌成功', '10000');
    }

    /**
     *  商品属性组
     * @param null
     * @return array
     */
    public function attributeGroup()
    {
        $data = $this->goodsNatureLogic->attributeGroup();
        $this->customizedResult(true, $data, '获取商品属性组成功', '10000');
    }

    /**
     *  商品属性名
     * @param null
     * @return array
     */
    public function attributeKey($group)
    {
        $data = $this->goodsNatureLogic->attributeKey($group);
        $this->customizedResult(true, $data, '获取商品属性名成功', '10000');
    }

    /**
     *  商品属性值
     * @param null
     * @return array
     */
    public function attributeValue($key)
    {
        $data = $this->goodsNatureLogic->attributeValue($key);
        $this->customizedResult(true, $data, '获取商品属性值成功', '10000');
    }

    /**
     *  添加商品
     * @param Object
     * @return customizedResult
     */
    public function add()
    {
        $data = $this->goodsSingleLogic->add();
        if ($data) {
            $this->customizedResult(true, $data, '添加商品成功', '10000');
        }
    }
}