<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 17:02
 */
namespace app\admin\controller;

use \think\Loader;
use app\common\controller\Auth as AuthController;

class Wechat extends AuthController
{
    protected $wechatConfigLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->wechatConfigLogic = Loader::model('WechatConfig', 'logic');
    }

    /**
     *  获取微信默认配置
     * @param null
     * @return array
     */
    public function defaultConfig()
    {
        $data = $this->wechatConfigLogic->config();
        if (!empty($data)) {
            $this->customizedResult(true, $data, '获取微信配置成功', '10000');
        }
        $this->customizedResult(false, null, '获取微信配置失败', '10100');
    }

    /**
     *  编辑微信公众号配置
     * @param $id ,配置id,$type,配置项,$val,配置值
     */
    public function editConfig($id, $type, $val)
    {
        $input = [
            'id' => $id,
            'value' => $val
        ];
        if ($type === 'token') {
            $data = $this->wechatConfigLogic->editToken($input);
        } else if ($type === 'appid') {
            $data = $this->wechatConfigLogic->editAppid($input);
        } else if ($type === 'appsecret') {
            $data = $this->wechatConfigLogic->editAppsecret($input);
        } else {
            $data = 'type为非法值';
        }
        if ($data) {
            $this->customizedResult(true, null, '修改微信配置' . $type . '成功', '10000');
        }
        $this->customizedResult(false, null, '修改微信配置' . $type . '失败', '10100');
    }
}