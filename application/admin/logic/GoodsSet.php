<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 12:16
 */

namespace app\admin\logic;

use think\Model;
use \think\Loader;
use app\common\model\Goods as GoodsModel;

class GoodsSet extends Model
{
    protected $goodsModel;

    protected function initialize()
    {
        parent::initialize();
        $this->goodsModel = Loader::model('Goods', 'model');
    }

    /**
     *  获取全部商品列表
     * @param null
     * @return array
     */
    public function ls()
    {
        $list = GoodsModel::all(function ($query) {
            $query->field([
                'id',
                'goods_sn' => 'goodsSn',
                'goods_name' => 'goodsName',
                'goods_desc' => 'goodsDesc',
                'list_pic_url' => 'listPicUrl'
            ])->order('id', 'asc');
        });
        return $list;
    }

    /**
     *  获取上架中的商品
     * @param null
     * @return array
     */
    public function onsaleLs()
    {
        $list = GoodsModel::all(['is_onsale' => 1]);
        return $list;
    }

    /**
     *  获取属于新品的商品
     * @param null
     * @return array
     */
    public function newLs()
    {
        $list = GoodsModel::all(['is_New' => 1]);
        return $list;
    }
}