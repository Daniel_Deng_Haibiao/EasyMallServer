<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 12:04
 */
namespace app\admin\logic;

use think\Model;
use \think\Loader;
use app\common\utils\Encrypt as EncryptUtil;
use app\common\model\Administrator as AdministratorModel;

class UserEntry extends Model
{
    protected $administratorModel;

    public function initialize()
    {
        parent::initialize();
        $this->administratorModel = Loader::model('administrator', 'model');
    }

    public function login($val)
    {
        $encryptPassword = EncryptUtil::password($val['password']);
        $administrator = AdministratorModel::get(['account' => $val['account'], 'password' => $encryptPassword]);
        if (isset($administrator->id)) {
            $data = [
                'userId' => $administrator->id
            ];
            return $data;
        }
        return false;
    }
}
