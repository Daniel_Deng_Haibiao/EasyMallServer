<?php
namespace app\admin\logic;

use think\Model;
use \think\Loader;
use app\common\model\Goods as GoodsModel;

class Goods extends Model
{
    protected $goodsModel;

    protected function initialize()
    {
        parent::initialize();
        $this->goodsModel = Loader::model('Goods', 'model');
    }

    /**
     *  获取全部商品列表
     * @param null
     * @return array
     */
    public function fullList()
    {
        $list = GoodsModel::all(function ($query) {
            $query->field([
                'id',
                'goods_sn' => 'goodsSn',
                'goods_name' => 'goodsName',
                'goods_desc' => 'goodsDesc',
                'list_pic_url' => 'listPicUrl'
            ])->order('id', 'asc');
        });
        return $list;
    }

    /**
     *  获取上架中的商品
     * @param null
     * @return array
     */
    public function onsaleList()
    {
        $list = GoodsModel::all(['is_onsale' => 1]);
        return $list;
    }

    /**
     *  获取属于新品的商品
     * @param null
     * @return array
     */
    public function newList()
    {
        $list = GoodsModel::all(['is_New' => 1]);
        return $list;
    }

    /**
     *  获取商品分类
     * @param null
     * @return array
     */
    public function category()
    {
        $list = $this->goodsModel->category();
        $tree = make_tree($list );
        return $tree;
    }

    /**
     * @descrription 商品品牌
     * @param null
     * @return array
     */
    public function brand()
    {
        $list = $this->goodsModel->brand();
        return $list;
    }

    /**
     *  商品属性
     * @param null
     * @return array
     */
    public function attribute()
    {
        $list = $this->goodsModel->attribute();
        return $list;
    }
}