<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 12:17
 */

namespace app\admin\logic;

use think\Model;
use \think\Loader;
use app\common\utils\Image as ImageUtil;


class GoodsSingle extends Model
{
    protected $goodsModel;

    protected function initialize()
    {
        parent::initialize();
        $this->goodsModel = Loader::model('Goods', 'model');
    }

    /**
     *  添加商品
     * @param Object
     * @return Bool
     */
    public function add()
    {
        $this->goodsModel->goods_sn = request()->param('sn');
        $this->goodsModel->goods_name = request()->param('name');
        $this->goodsModel->market_price = request()->param('marketPrice');
        $this->goodsModel->brand_id = request()->param('brand');
        $image = new ImageUtil();
        $image->loadBase64(request()->param('primaryPic'))->save();
        if ($image->imageUrl) {
            $this->goodsModel->primary_pic_url = $image->imageUrl;
            if ($this->goodsModel->save()) {
                return true;
            };
        }
    }
}