<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 17:04
 */

namespace app\admin\logic;

use think\Model;
use \think\Loader;
use app\common\model\WechatConfig as WechatConfigModel;

class WechatConfig extends Model
{
    protected $wechatConfigModel;

    public function initialize()
    {
        parent::initialize();
        $this->wechatConfigModel = Loader::model('WechatConfig', 'model');
    }

    public function config()
    {
        $config = WechatConfigModel::get(['is_default' => 1]);
        return $config;
    }

    /**
     *  修改微信公众号token配置
     * @param $val ,配置信息
     * @return bool
     */
    public function editToken($val)
    {
        $id = $val['id'];
        $value = $val['value'];
        $config = WechatConfigModel::get($id);
        $config->token = $value;
        if ($config->save()) {
            return true;
        }
        return false;
    }

    /**
     *  修改微信公众号appid配置
     * @param $val ,配置信息
     * @return bool
     */
    public function editAppid($val)
    {
        $id = $val['id'];
        $value = $val['value'];
        $config = WechatConfigModel::get($id);
        $config->appid = $value;
        if ($config->save()) {
            return true;
        }
        return false;
    }

    /**
     *  修改微信公众号appsecret配置
     * @param $val ,配置信息
     * @return bool
     */
    public function editAppsecret($val)
    {
        $id = $val['id'];
        $value = $val['value'];
        $config = WechatConfigModel::get($id);
        $config->appsecret = $value;
        if ($config->save()) {
            return true;
        }
        return false;
    }
}