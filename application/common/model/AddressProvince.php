<?php

namespace app\common\model;

use think\Model;
use think\Db;

class AddressProvince extends Model
{
    /**
     *  模型关联
     */
    public function addressCity()
    {
        return $this->hasMany('addressCity', 'province_id', 'province_id');
    }

    /**
     *  全国地址列表
     * @param null
     * @return array
     */
    public function nationAddessList()
    {
        $list = Db::field('province_name as name,province_id as value,0 as parent')
            ->table('em_address_province')
            ->union('SELECT city_name AS name,city_id AS value, province_id as parent FROM em_address_city')
            ->union('SELECT area_name AS name,area_id AS value, city_id as parent FROM em_address_area')
            ->select();
        return $list;
    }
}
