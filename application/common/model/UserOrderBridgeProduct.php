<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/3/6
 * Time: 17:31
 */

namespace app\common\model;

use think\Db;
use think\Model;

class UserOrderBridgeProduct extends Model
{
    protected static function init()
    {
        UserOrderBridgeProduct::event('before_insert', function ($userOrderBridgeProduct) {
            $orderId = $userOrderBridgeProduct->order_id;
            $productId = $userOrderBridgeProduct->product_id;
            $productNum = $userOrderBridgeProduct->product_num;
            $data = DB::table('em_goods_product')
                ->where(['id' => $productId])
                ->find();
            $productPrice = $data['market_price'];
            DB::table('em_user_order')
                ->where(['id' => $orderId])
                ->update([
                    'order_price' => bcmul((int)$productNum, (float)$productPrice, 2),
                    'actual_price' => bcmul((int)$productNum, (float)$productPrice, 2)
                ]);
        });
    }
}