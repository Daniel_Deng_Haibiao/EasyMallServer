<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 9:53
 */
namespace app\common\model;

use think\Model;

class GoodsAttributeKey extends Model
{
    public function goodsAttributeValue()
    {
        return $this->hasMany('goodsAttributeValue', 'attribute_key_id', 'id');
    }
}