<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 9:52
 */
namespace app\common\model;

use think\Model;

class GoodsAttributeGroup extends Model
{
    public function goodsAttributeKey()
    {
        return $this->hasMany('goodsAttributeKey', 'attribute_group_id', 'id');
    }
}