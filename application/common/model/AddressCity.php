<?php

namespace app\common\model;

use think\Model;

class AddressCity extends Model
{
    public function addressArea()
    {
        return $this->hasMany('addressArea', 'city_id', 'city_id');
    }
}
