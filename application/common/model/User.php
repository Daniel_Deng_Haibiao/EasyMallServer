<?php

namespace app\common\model;

use think\Model;
use think\Db;
use think\Request;

class User extends Model
{
    /**
     *  数据自动赋值
     */
    protected static function init()
    {
        User::afterInsert(function ($user) {
            //em_user_account
            $userAccountData = array(
                'user_id' => $user->id,
                'status' => 1
            );
            Db::table('em_user_account')->insert($userAccountData);
            //em_user_level
            $defaultLevel = Db::table('em_user_level_define')
                ->where(['limit' => 0])
                ->find();
            $userLevelData = array(
                'level_value' => 0,
                'level_define_id' => $defaultLevel['id'],
                'user_id' => $user->id
            );
            Db::table('em_user_level')->insert($userLevelData);
            //em_user_peas
            $userPeasData = array(
                'user_id' => $user->id
            );
            Db::table('em_user_peas')->insert($userPeasData);
        });
    }

    /**
     *  模型关联
     */
    public function userAuth()
    {
        return $this->hasMany('userAuth', 'user_id');

    }

    public function userAccount()
    {
        return $this->hasOne('userAccount', 'user_id');

    }

    public function userNeteaseIm()
    {
        return $this->hasOne('userNeteaseIm', 'user_id');

    }

    public function userPeas()
    {
        return $this->hasOne('userPeas', 'user_id');

    }

    public function userLevel()
    {
        return $this->hasOne('userLevel', 'user_id');

    }

    public function userShippingAddress()
    {
        return $this->hasMany('userShippingAddress', 'user_id');

    }

    public function goodsCollect()
    {
        return $this->belongsToMany('goods', 'UserBridgeGoodsCollect');
    }

    /**
     *  常用方法定义
     */

    // ,用户是否已经注册，@identity，用户登录帐号，@return bool
    public function isRegist($identity)
    {
        $account = Db::table('em_user_auth')
            ->where(['identifier' => $identity])
            ->find();
        if (!empty($account)) {
            return true;
        }
    }

    // ,获取当前登录用户实例对象，@param null，@return object
    public function currentUser()
    {
        $userId = Request::instance()->current_user->user_id;
        $user = self::get($userId);
        if (isset($user)) {
            return $user;
        }
    }

    // ,获取当前登录用户地址列表，@param null，@return array
    public function addressList()
    {
        $userId = Request::instance()->current_user->user_id;
        $list = Db::table('em_user_shipping_address')
            ->field('a.id as addressId, a.is_default as isDefault, b.province_name as province, c.city_name as city, d.area_name as area, a.address as address, a.phone as phone, a.receiver as receiver')
            ->alias('a')
            ->join('em_address_province b', 'a.province_id = b.province_id', 'LEFT')
            ->join('em_address_city c', 'a.city_id = c.city_id', 'LEFT')
            ->join('em_address_area d', 'a.area_id = d.area_id', 'LEFT')
            ->where('a.user_id', $userId)
            ->order('a.create_time desc')
            ->select();
        return $list;
    }

    // 用户购物车列表
    public function cart()
    {
        $userId = Request::instance()->current_user->user_id;
        $list = Db::table('em_user_cart')
            ->field('a.id as id, a.product_num as productNum, f.name as productAttName, g.name as productAttValue, b.market_price as productPrice, b.id as productId, c.id as goodsId, c.goods_brief as goodsBrief, c.primary_pic_url as goodsImgUrl, d.id as shopId, d.shop_name as shopName')
            ->alias('a')
            ->join('em_goods_product b', 'a.product_id = b.id', 'LEFT')
            ->join('em_goods c', 'b.goods_id = c.id', 'LEFT')
            ->join('em_seller_shop d', 'c.shop_id = d.id', 'LEFT')
            ->join('em_goods_product_bridge_attribute e', 'b.id = e.product_id', 'LEFT')
            ->join('em_goods_attribute_key f', 'e.attribute_key_id = f.id', 'LEFT')
            ->join('em_goods_attribute_value g', 'e.attribute_value_id = g.id', 'LEFT')
            ->where('a.user_id', $userId)
            ->select();
        return $list;
    }
}
