<?php

namespace app\common\model;

use think\Model;
use think\Db;

class Goods extends Model
{
    protected static function init()
    {
        Goods::afterInsert(function ($goods) {
            //goods_bridge_category
            $categoryData = array(
                'goods_id' => $goods->id,
                'category_id' => request()->param('category')
            );
            Db::table('em_goods_bridge_category')->insert($categoryData);
            //goods_bridge_attribute
            $attributeData = array(
                'goods_id' => $goods->id,
                'attribute_key_id' => request()->param('attributeKey'),
                'attribute_value_id' => request()->param('attributeValue'),
            );
            Db::table('em_goods_bridge_attribute')->insert($attributeData);
        });
    }

    public function goodsProduct()
    {
        return $this->hasMany('goodsProduct', 'goods_id');

    }
}