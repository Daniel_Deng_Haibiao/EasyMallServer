<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 9:54
 */

namespace app\common\model;

use think\Model;
use think\Db;

class GoodsBrand extends Model
{
    /**
     * @descrription 商品品牌
     * @param null
     * @return array
     */
    public function ls()
    {
        $list = DB::table('em_goods_brand')
            ->field('id as value, name as label')
            ->select();
        return $list;
    }
}