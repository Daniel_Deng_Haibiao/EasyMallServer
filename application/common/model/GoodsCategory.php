<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 9:57
 */

namespace app\common\model;

use think\Model;
use think\Db;

class GoodsCategory extends Model
{
    /**
     *  商品分类
     * @param null
     * @return array
     */
    public function ls()
    {
        $list = Db::table('em_goods_category')
            ->field('id as value, parent_id, name as label')
            ->select();
        return $list;
    }

}