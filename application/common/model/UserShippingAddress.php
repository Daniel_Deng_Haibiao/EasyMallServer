<?php

namespace app\common\model;

use think\Model;

class UserShippingAddress extends Model
{
    public function addressProvince()
    {
        return $this->belongsTo('addressProvince', 'province_id', 'province_id')->bind('province_name');

    }

    public function addressCity()
    {
        return $this->belongsTo('addressCity', 'city_id', 'city_id')->bind('city_name');

    }

    public function addressArea()
    {
        return $this->belongsTo('addressArea', 'area_id', 'area_id')->bind('area_name');

    }
}
