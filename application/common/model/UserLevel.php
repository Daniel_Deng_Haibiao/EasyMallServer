<?php

namespace app\common\model;

use think\Model;

class UserLevel extends Model
{
    public function userLevelDefine()
    {
        return $this->belongsTo('userLevelDefine', 'level_define_id');
    }
}
