<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 10:37
 */

namespace app\common\model;

use think\Db;
use think\Model;

class UserOrder extends Model
{
    protected static function init()
    {
        UserOrder::afterInsert(function ($userOrder) {
            $userOrder->expire_time = date("Y-m-d H:i:s",time()+86400);
            $userOrder->save();
        });
    }
    public function orderBridgeProduct()
    {
        return $this->hasMany('userOrderBridgeProduct', 'order_id');
    }
    public function goodsProduct()
    {
        return $this->belongsToMany('goodsProduct', 'UserOrderBridgeProduct');
    }
}