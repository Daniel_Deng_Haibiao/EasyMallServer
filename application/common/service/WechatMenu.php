<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 14:35
 */

namespace app\common\service;

use app\common\service\Wechat as WechatService;

class WechatMenu extends WechatService
{
    /**
     *  获取微信自定义菜单
     * @param null
     * @return array
     */
    public function menu()
    {
        $menu = new \WeChat\Menu($this->config);
        return $menu;
    }
}