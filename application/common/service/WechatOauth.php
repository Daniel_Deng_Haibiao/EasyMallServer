<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 11:36
 */

namespace app\common\service;

use app\common\service\Wechat as WechatService;

class WechatOauth extends WechatService
{
    /**
     *  获取用户access_token
     * @param $code 前端返回的code
     * @return access_token
     */
    protected function accessToken($code)
    {
        $_GET['code'] = $code;
        $wechat = new \WeChat\Oauth($this->config);
        $data = $wechat->getOauthAccessToken();
        return $data;
    }

    /**
     * @param $accessToken ,$openid
     *  获取用户信息
     * @return array
     */
    public function userInfo($code)
    {
        $wechat = new \WeChat\Oauth($this->config);
        $accessToken = $this->accessToken($code);
        if (!empty($accessToken)) {
            $data = $wechat->getUserInfo($accessToken['access_token'], $accessToken['openid'], 'zh_CN');
            return $data;
        }
        return false;
    }
}