<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 12:01
 */

namespace app\common\service;

use app\common\service\Wechat as WechatService;


class WechatReceive extends WechatService
{
    /**
     *  文本信息处理
     * @param $receive
     * @return
     */
    protected function receiveText($receive)
    {
        $receive->text('https://www.baidu.com')->reply();
    }

    /**
     *  图像信息处理
     * @param $receive
     * @return
     */
    protected function receiveImage($receive)
    {

    }

    /**
     *  地址信息处理
     * @param $receive
     * @return
     */
    protected function receiveLocation($receive)
    {

    }

    /**
     *  事件信息处理
     * @param $receive
     * @return
     */
    protected function receiveEvent($receive)
    {

    }

    /**
     *  微信服务器推送事件
     * @param null
     * @return null
     */
    public function receive()
    {
        $receive = new \WeChat\Receive($this->config);
        $type = $receive->getMsgType();
        switch ($type) {
            case 'text':
                $this->receiveText($receive);
                break;
            case 'image':
                $this->receiveImage($receive);
                break;
            case 'location':
                $this->receiveLocation($receive);
                break;
            case 'event':
                $this->receiveEvent($receive);
                break;
            default:
                break;
        }

    }
}