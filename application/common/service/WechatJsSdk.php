<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/23
 * Time: 11:36
 */

namespace app\common\service;

use app\common\service\Wechat as WechatService;

class WechatJsSdk extends WechatService
{
    /**
     *  js-sdk签名
     * @param $url
     * @return array
     */
    public function signature($url)
    {
        $wechat = new \WeChat\Script($this->config);
        $result = $wechat->getJsSign($url);
        return $result;
    }
}