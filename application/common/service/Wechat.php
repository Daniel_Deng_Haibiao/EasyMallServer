<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/2/28
 * Time: 13:40
 */

namespace app\common\service;

use think\Model;
use think\Db;

class Wechat extends Model
{
    protected $config;

    protected function initialize()
    {
        $config = Db::table('em_wechat_config')
            ->where(['is_default' => 1])
            ->find();
        $this->config = $config;
    }
}