<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2018/3/7
 * Time: 16:34
 */

namespace app\common\utils;

class BuildStr
{
    static function orderSn()
    {
        $letter = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $sn = $letter[intval(date('Y')) - 2018] .
            strtoupper(dechex(date('m'))) .
            date('d') . substr(time(), -5) .
            substr(microtime(), 2, 5) .
            sprintf('%02d', rand(0, 99));
        return $sn;
    }
}