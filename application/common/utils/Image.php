<?php
/**
 * Created by PhpStorm.
 * User: daniel.deng.haibiao
 * Date: 2017/12/25
 * Time: 10:54
 */

namespace app\common\utils;

class Image
{
    private $fileStream;
    public $imageUrl;
    public $ext;

    /*
     * 解压base64图片
     * */
    private function decodeBase64($value)
    {
        $arrBase64 = explode(',', $value);
        $decondeBase64 = base64_decode($arrBase64[1]);
        $this->fileStream = $decondeBase64;
    }

    /*
     * 保存图片
     * */
    public function save($name = null)
    {
        //文件名
        $imageName = !isset($name) ? date('YmdHis') . rand(0, 9999) . '.' . $this->ext : $name . $this->ext;
        //文件地址
        $imageUrl = '/static/images/' . $imageName;
        //目录写入
        if (!is_dir(dirname('.' . $imageUrl))) {
            mkdir(dirname('.' . $imageUrl));
            //linux可能存在写入权限的问题chmod()/umask()
        }
        if (file_put_contents('.' . $imageUrl, $this->fileStream)) {
            $this->imageUrl = $imageUrl;
        }
        return $this;
    }

    /*
     * 加载base64图片
     * */
    public function loadBase64($value)
    {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $value, $result)) {
            //后缀
            $this->ext = $result[2];
            $this->decodeBase64($value);
        }
        return $this;
    }
}