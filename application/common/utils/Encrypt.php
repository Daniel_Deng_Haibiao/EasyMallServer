<?php
/**
 * Created by PhpStorm.
 * User: daniel.deng.haibiao
 * Date: 2017/12/21
 * Time: 14:58
 */

namespace app\common\utils;

use think\Config;

class Encrypt
{
    static function password($str = '')
    {
        $encryptKey = Config::get('customize')['secretKey'];
        $md5Str = md5($str . $encryptKey);
        return substr($md5Str, 0, 5) . $md5Str . substr($md5Str, -1, 5);
    }
}