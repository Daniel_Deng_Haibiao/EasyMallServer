<?php
namespace app\common\validate;
use think\Validate;

class User  extends Validate
{
    //规则
    protected $rule = [
        'type' => 'require',
        'account'  =>  'require',
        'password' =>  'require|max:12'
    ];
    //规则信息
    protected $message = [
        'type.require'  =>  '登录类型必填',
        'account.require' =>  '帐号必填',
        'password.require' =>  '密码必填',
    ];
    //场景
    protected $scene = [
        'weixin'  =>  ['type','account'],
        'regist'  =>  ['account','password'],
        'login'  =>  ['account','password']
    ];
}