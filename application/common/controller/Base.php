<?php
/**
 * Created by PhpStorm.
 * User: Neho
 * Date: 2017/11/12
 * Time: 12:52
 */

namespace app\common\controller;

use think\Controller;
use think\exception\HttpResponseException;
use think\Response;


class Base extends Controller
{
    public function _initialize()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-type:text/html;charset=utf-8");
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Token');
        header('Access-Control-Allow-Methods: GET, POST, PUT');
        header('Access-Control-Expose-Headers : Token');
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }
    }

    protected function customizedResult($success = true, $data, $errorMsg = '', $errorCode = '', array $header = [])
    {
        $result = [
            'success' => $success,
            'data' => $data,
            'error_code' => $errorCode,
            'error_msg' => $errorMsg
        ];
        $type = "json";
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }
}